
var http = require('http');
var url = require('url');
var fs = require('fs');
var username = "user1";


var server = "192.168.30.249";
var webAppId = "webapp-id-example";
var localPort = 8000;

var serverUrl = "http://" + server + ":8080/gateway";
var fcsdkJsUrl = serverUrl + "/fusion-client-sdk.js";
var fcsdkAdapterUrl = serverUrl + "/adapter.js";

var generatedUser = 1;

var replaceContent = function(content, sessionId) {
    var parsedContent = content;
    parsedContent = parsedContent.replace("%SESSIONID%", sessionId);
    parsedContent = parsedContent.replace("%ADAPTER_URL%", fcsdkAdapterUrl);
    parsedContent = parsedContent.replace("%FCSDK_URL%", fcsdkJsUrl);
    return parsedContent;
}

var handleRequest = function(request, response) {
    var urlParams = url.parse(request.url, true).query;
    var username = urlParams.user;

    // Check if this is a request for the page icon
    if ((/^\/favicon\.ico/).test(request.url)) {
        console.log('Favicon requested, not generating session');
        response.writeHead(204);
        response.end();
        return;
    }

    // Redirect with a generated user if one was not provided
    if (!username || username.length === 0) {
        console.log("No username provided, redirecting");
        response.writeHead(302, {
            Location: '?user=user' + generatedUser
        });
        generatedUser++;
        response.end();
        return;
    }

    console.log("Processing request, user [" + urlParams.user + "]");

    var sessionDesc = {
        "webAppId" : webAppId,
        "voice": {
            "username" : username,
            "domain" : server
        }
    };

    var sessionDescJson = JSON.stringify(sessionDesc);

    var options = {
        hostname: server,
        port: 8080,
        path: '/gateway/sessions/session',
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            'Content-Length':sessionDescJson.length,
	    'Access-Control-Allow-Origin' : '*'
        }
    };

    var processResponse = function(resp) {
        resp.setEncoding('utf-8');
        resp.on('data', function(chunk) {
            var sessionId = JSON.parse(chunk).sessionid;

            console.log("Providing SessionID: [" + sessionId + "]");
            var content = fs.readFileSync('./index.html', {encoding: 'utf-8'});
            var parsedContent = replaceContent(content, sessionId);

            response.writeHead(200, {'Content-Type':'text/html'},{'Access-Control-Allow-Origin' : '*'});
            response.end(parsedContent, 'utf-8');
        });
    };

    var req = http.request(options, processResponse);

    req.on('error', function(e) {
        console.log("Error: " + e.message);
    });

    req.end(JSON.stringify(sessionDesc), 'utf-8');
};

http.createServer(handleRequest).listen(localPort);
console.log('Server running on port: [' + localPort + ']');
console.log('Server running at http://127.0.0.1:8000/');
